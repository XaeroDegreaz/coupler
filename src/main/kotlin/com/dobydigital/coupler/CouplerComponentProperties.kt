package com.dobydigital.coupler

class CouplerComponentProperties(
    val type : String,
    val configuration : Map<String, Any>,
    val outputs : List<String> = listOf())
