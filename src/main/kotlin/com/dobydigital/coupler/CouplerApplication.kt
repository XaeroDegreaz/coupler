package com.dobydigital.coupler

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication

@SpringBootApplication
@EnableConfigurationProperties(CouplerPipelineConfiguration::class)
class CouplerApplication

fun main(args : Array<String>)
{
    runApplication<CouplerApplication>(*args)
}
