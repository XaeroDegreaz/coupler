package com.dobydigital.coupler.api

import org.springframework.http.HttpMethod

class ApiIngressComponentProperties(val endpoint : String, val method : HttpMethod, val consumes : String)