package com.dobydigital.coupler.api

import com.dobydigital.coupler.CouplerComponent
import com.dobydigital.coupler.CouplerPipelineConfiguration
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.convertValue
import org.apache.http.HttpStatus
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationContext
import org.springframework.http.HttpMethod
import org.springframework.stereotype.Component
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter
import java.util.stream.Collectors
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * This component is an entry point to trigger a pipeline.
 *
 * For now we just pass some simple JSON data to it and pass it down to the next chain in the pipeline.
 *
 * I'd prefer to create a RestController annotated bean on the fly so that I don't have to manage much plumbing, unfortunately
 * the most customizable @RequestMapping annotation uses an enum type for HTTP method, so can't pull that directly from spring boot config unfortunately :(
 */
@Component
class ApiIngressComponent(
    private val couplerPipelineConfiguration : CouplerPipelineConfiguration,
    private val objectMapper : ObjectMapper,
    private val applicationContext : ApplicationContext) : HandlerInterceptorAdapter(), WebMvcConfigurer
{
    companion object
    {
        private val log : Logger = LoggerFactory.getLogger(ApiIngressComponent::class.java)
    }

    override fun addInterceptors(registry : InterceptorRegistry)
    {
        registry.addInterceptor(this)
    }

    override fun preHandle(request : HttpServletRequest, response : HttpServletResponse, handler : Any) : Boolean
    {
        //# This needs to be refactored into a separate component like the rest of the components instead of embedded in the top-level
        //# handler domain
        try
        {
            //# Is there an api ingress component configured with the incoming request uri?
            val couplerComponent = couplerPipelineConfiguration
                .components
                .values
                .filter { it.type == javaClass.name }
                .first {
                    //# TODO - Work with the configuration binder to programatically generate config pojos
                    val apiIngressComponentProperties = objectMapper.convertValue<ApiIngressComponentProperties>(it.configuration)
                    apiIngressComponentProperties.endpoint == request.requestURI
                }
            val properties = objectMapper.convertValue<ApiIngressComponentProperties>(couplerComponent.configuration)
            //# Is it for the same http method?
            if (HttpMethod.resolve(request.method) == properties.method)
            {
                val body = request
                    .reader
                    .lines()
                    .collect(Collectors.joining(System.lineSeparator()))
                //# Move to the next output in the pipeline chain
                val outputComponentName = couplerComponent.outputs[0]
                val outputComponentProperties = couplerPipelineConfiguration.components[outputComponentName]!!
                val bean = applicationContext.autowireCapableBeanFactory.createBean(Class.forName(outputComponentProperties.type)) as CouplerComponent
                val output = bean.apply(outputComponentName, outputComponentProperties, body)
                response.status = if (output != null)
                {
                    //# TODO - yes yes, we don't always know it's gonna be a string getting sent back. just for demonstration purposes
                    response.writer.write(output.toString())
                    HttpStatus.SC_OK
                }
                else
                {
                    HttpStatus.SC_NO_CONTENT
                }
            }
            else
            {
                response.status = HttpStatus.SC_NOT_ACCEPTABLE
            }
        }
        catch (e : NoSuchElementException)
        {
            response.status = HttpStatus.SC_NOT_FOUND
            log.error("404", e)
        }
        catch (e : Exception)
        {
            response.status = HttpStatus.SC_INTERNAL_SERVER_ERROR
            log.error("500", e)
        }
        return false
    }
}