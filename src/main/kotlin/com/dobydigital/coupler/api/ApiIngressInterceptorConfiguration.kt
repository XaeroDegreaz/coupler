package com.dobydigital.coupler.api

import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

//@Configuration
class ApiIngressInterceptorConfiguration(private val apiHandlerInterceptorComponent : ApiIngressComponent) : WebMvcConfigurer
{
    override fun addInterceptors(registry : InterceptorRegistry)
    {
        registry.addInterceptor(apiHandlerInterceptorComponent)
        // am i a good enough typist to code while in vr?
    }
}

























