package com.dobydigital.coupler.http

import org.springframework.http.HttpMethod

class HttpCallComponentProperties(val method : HttpMethod, val url : String)