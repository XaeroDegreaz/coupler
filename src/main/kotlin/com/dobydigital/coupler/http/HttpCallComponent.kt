package com.dobydigital.coupler.http

import com.dobydigital.coupler.CouplerComponent
import com.dobydigital.coupler.CouplerComponentProperties
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.convertValue
import com.fasterxml.jackson.module.kotlin.readValue
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.ApplicationContext
import java.util.regex.Pattern

/**
 * This component simply makes http calls somewhere.
 * It does variable substitution depending on inputs that are passed in from other components in the pipeline.
 *
 * It's simple for now (variable substitution in url) but could get really fancy if you want
 */
class HttpCallComponent(
    private val objectMapper : ObjectMapper,
    private val restTemplateBuilder : RestTemplateBuilder,
    private val applicationContext : ApplicationContext) : CouplerComponent
{
    companion object
    {
        private val pattern : Pattern = Pattern.compile("\\$\\{(.+?)}")
    }

    override fun apply(componentName : String, componentProperties : CouplerComponentProperties, input : Any) : String?
    {
        val properties = objectMapper.convertValue<HttpCallComponentProperties>(componentProperties.configuration)//HttpCallComponentProperties(componentProperties.configuration)
        val buffer = StringBuffer(properties.url)
        val matcher = pattern.matcher(buffer)
        val parts = mutableSetOf<String>()
        while (matcher.find())
        {
            parts.add(matcher.group(1))
        }
        //# TODO - Abstract this out instead of copy and pasting around :D
        val map = when (input)
        {
            is String ->
            {
                objectMapper.readValue(input)
            }
            is MutableMap<*, *> ->
            {
                input as MutableMap<String, Any>
            }
            else ->
            {
                TODO("Only strings and map values are currently supported")
            }
        }
        parts.forEach {
            val part = it
            val search = "\${$part}"
            val index = buffer.indexOf(search)
            buffer.replace(index, index + search.length, map[part].toString())
        }
        val responseEntity = restTemplateBuilder
            .build()
            .exchange(buffer.toString(), properties.method, null, String::class.java)
        //# Google's HTML output should be stored in this
        val body = responseEntity.body
        //# Move to the next item in the chain. For this initial implementation, and current setup, let's just stop until we abstract chaining out
        return body
    }
}