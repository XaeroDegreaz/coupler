package com.dobydigital.coupler.transformation

import com.fasterxml.jackson.annotation.JsonProperty

class JsonMapperComponentProperties(
    @JsonProperty("input-to-output-mappings")
    val inputToOutputMappings : Map<String, MappingConfig>)
{
    class MappingConfig(
        @JsonProperty("new-key-name")
        val newKeyName : String)
}