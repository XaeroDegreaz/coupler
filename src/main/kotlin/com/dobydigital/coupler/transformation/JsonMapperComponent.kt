package com.dobydigital.coupler.transformation

import com.dobydigital.coupler.CouplerComponent
import com.dobydigital.coupler.CouplerComponentProperties
import com.dobydigital.coupler.CouplerPipelineConfiguration
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.convertValue
import com.fasterxml.jackson.module.kotlin.readValue
import org.springframework.context.ApplicationContext

/**
 * This component takes String JSON, or a fully hydrated map input, and renames keys based on configuration.
 *
 * A use case could be if you're reaching out to some 3rd party API to pull back some JSON, and you want to pass those values to some internal API,
 * but your internal API domain model has different field names.
 */
class JsonMapperComponent(
    private val objectMapper : ObjectMapper,
    private val couplerPipelineConfiguration : CouplerPipelineConfiguration,
    private val applicationContext : ApplicationContext) : CouplerComponent
{
    override fun apply(componentName : String, componentProperties : CouplerComponentProperties, input : Any) : Any?
    {
        //# TODO - Abstract this out instead of copy and pasting around :D
        val map = when (input)
        {
            is String ->
            {
                objectMapper.readValue(input)
            }
            is MutableMap<*, *> ->
            {
                input as MutableMap<String, Any>
            }
            else ->
            {
                TODO("Only strings and map values are currently supported")
            }
        }
        val jsonMapperComponentProperties = objectMapper.convertValue<JsonMapperComponentProperties>(componentProperties.configuration)
        jsonMapperComponentProperties.inputToOutputMappings.forEach {
            //# Add new key, with old key's values
            map[it.value.newKeyName] = map[it.key]!!
            //# Remove old key
            map.remove(it.key)
        }
        //# Move to the next output in the pipeline chain
        //# TODO - Abstract this out instead of copy and pasting around :D
        val outputComponentName = componentProperties.outputs[0]
        val outputComponentProperties = couplerPipelineConfiguration.components[outputComponentName]!!
        val bean = applicationContext.autowireCapableBeanFactory.createBean(Class.forName(outputComponentProperties.type)) as CouplerComponent
        //# Potentially return the goods somewhere
        return bean.apply(outputComponentName, outputComponentProperties, map)
    }
}