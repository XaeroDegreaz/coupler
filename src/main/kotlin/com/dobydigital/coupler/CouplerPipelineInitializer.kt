package com.dobydigital.coupler

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationListener
import org.springframework.context.event.ContextRefreshedEvent

//@Component
class CouplerPipelineInitializer(private val couplerPipelineConfiguration : CouplerPipelineConfiguration) : ApplicationListener<ContextRefreshedEvent>
{
    companion object
    {
        private val log : Logger = LoggerFactory.getLogger(CouplerPipelineInitializer::class.java)
    }

    override fun onApplicationEvent(event : ContextRefreshedEvent)
    {
        val autowireCapableBeanFactory = event.applicationContext.autowireCapableBeanFactory
        couplerPipelineConfiguration.components.forEach {
            try
            {
                val componentName = it.key
                val couplerComponent = it.value
                val bean = autowireCapableBeanFactory.createBean(Class.forName(couplerComponent.type))
                autowireCapableBeanFactory.initializeBean(bean, it.key)
            }
            catch (e : Exception)
            {
                log.error("Failed to autowire component", e)
            }
        }
    }
}