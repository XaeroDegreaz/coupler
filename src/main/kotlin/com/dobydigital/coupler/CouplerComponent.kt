package com.dobydigital.coupler

interface CouplerComponent
{
    /**
     * TODO Think of a better name later
     *
     * Do some work inside a component and potentially return some value
     */
    fun apply(componentName : String, componentProperties : CouplerComponentProperties, input : Any) : Any?
}