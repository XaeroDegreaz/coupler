package com.dobydigital.coupler

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

@ConstructorBinding
@ConfigurationProperties("coupler")
class CouplerPipelineConfiguration(
    @NotNull
    val environment : String,
    @NotEmpty
    val components : Map<String, CouplerComponentProperties>)