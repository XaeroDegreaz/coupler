You can kick off the pipeline by sending a `POST` request to `http://localhost:8080/test-endpoint`
with the contents `{"test" : "value"}`.